import React from "react";
import "./App.css";
import AssetChart from "./components/visualizations/assetChart";
import AssetPie from "./components/visualizations/assetPie";
import {Grid, Row, Col} from "react-flexbox-grid"; 

const App: React.FunctionComponent = () => {
  return (
    <div className="App">
      <Grid fluid>
        <Row>
          <Col xs>
            <div>
              <AssetPie/>
            </div>
          </Col>
          <Col xs>
            <div>
              <AssetChart/>
            </div>
          </Col>
        </Row>
      </Grid>
    </div>
  );
};

export default App;