import axios from "axios";
import { Line } from "react-chartjs-2";
import React, { useState, useEffect } from "react";
import logger from "../../util/logger";
import Asset from "../../types/Asset.inter";

const emptyAssetData = (): Asset => ({
 name: "",
 type: "",
 data: {
  labels: [],
  datasets: [],
 }
});

const AssetChart : React.FunctionComponent = () => {

 // Refer to info on: https://github.com/jerairrest/react-chartjs-2/blob/master/example/src/components/line.js
 const [assetData, setAssetData] = useState<Asset>(emptyAssetData);

 async function assetDataRequest() {
  await axios.get("http://localhost:8081/asset/price?name=GDX&type=STOCK&timeframe=month")
  .then(json => {
   let currentAsset: Asset = {
    name: "GDX", 
    type: "STOCK", 
    data: {
     labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
     datasets: [{
      label: "GDX",
      fill: false,
      lineTension: 3,
      backgroundColor: "rgba(75,192,192,0.4)",
      borderColor: "rgba(75,192,192,1)",
      borderCapStyle: "butt",
      borderJoinStyle: "miter",
      pointBorderColor: "rgba(75,192,192,1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 4,
      pointHoverRadius: 2, 
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 3,
      pointRadius: 2,
      pointHitRadius: 3,
      data: json.data.data
     }]
    }
   }
   setAssetData(currentAsset);
  });
 }

 useEffect(() => {
  assetDataRequest();
 }, []);

 /* TODO: research the proper structure for this object to show data in expected structure
 
 const lineOptions = {
  scales: {
   xAxes: [{
    type: "time",
    distribution: "linear",
    time: {
     unit: "day",
     displayFormats: {
      day: "D",
     }
    }
   }]
  }
 };
 logger.info("new data: %s", JSON.stringify(assetData));
 */

 return (
  <div>
   <Line
    data={assetData.data} 
    />
  </div>
 );
}

export default AssetChart;