import React, { useState, useEffect } from "react";
import Asset from "../../types/Asset.inter";
import { Pie } from "react-chartjs-2";

const emptyPieData = (): Asset => ({
 name: "",
 type: "",
 data: {
  labels: [],
  datasets: [],
 }
});

const AssetPie: React.FunctionComponent = () => {

    const [assetPieData, setPieData] = useState<Asset>(emptyPieData);

    useEffect(() => {
        let currentPieData = {
            name: "Current asset allocations",
            type: "All",
            data: {
                labels: ["Stock", "Currency", "Precious metals"],
                datasets: [{
                    data: [1000, 4500, 15000],
                    backgroundColor: [
                        "#3fa7d6",
                        "#fac05e",
                        "#59cd90",
                    ],
                    hoverBackgroundColor: [
                        "#49c5ff",
                        "#f9a004",
                        "#49ff9e",
                    ],
                }],
            }
        }
        setPieData(currentPieData);
    }, []);

    return (
        <Pie 
            data={assetPieData.data}
        />
    );
}

export default AssetPie;