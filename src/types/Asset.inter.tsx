import VisualizationData from "./VisualizationData.inter";

export default interface Asset {
 name: string;
 type: string;
 data: VisualizationData;
}