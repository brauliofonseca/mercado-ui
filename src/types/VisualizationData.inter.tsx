export default interface VisualizationData {
 labels: string[],
 datasets: object[],
}